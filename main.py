import gensim
from bs4 import BeautifulSoup
import requests
import json
import urllib
import time
import threading

def loading():
    while True:
        print("\rThinking.  ", end="")
        time.sleep(0.5)
        print("\rThinking.. ", end="")
        time.sleep(0.5)
        print("\rThinking...", end="")
        time.sleep(0.5)
        global guessing
        if not guessing:
            break


def greeting():
    response = requests.get('https://semantle.ishefi.com')
    soup = BeautifulSoup(response.text, features='html.parser')
    puzzle_number = (str(soup.find(id="puzzleNumber").string))
    print("Welcome to davka!")
    time.sleep(2)
    print("I will now attempt to solve the Hebrew Semantle (puzzle #" + puzzle_number + ").")
    time.sleep(2)




def guesser():
    model = gensim.models.Word2Vec.load('./wiki_tokenized_model/model.mdl').wv
    words = []
    for word in model.key_to_index.keys():
        if all(ord('א') <= ord(c) <= ord('ת') for c in word) and len(word) > 1:
            words.append(word)

    first_guess = words[0]
    words.pop(0)

    page = 'https://semantle.ishefi.com/api/distance?word='
    secret = ''
    response = requests.get(page + first_guess)
    soup = BeautifulSoup(response.text, features='html.parser')

    first_guess_similarity = json.loads(soup.text)['similarity'] / 100

    candidates = [w for w in words if
                first_guess_similarity - 0.2 <= model.similarity(first_guess, w) <= first_guess_similarity + 0.2]
    guess_counter = 0

    while True:
        guess = urllib.parse.quote(candidates[0])
        guess_counter += 1
        #print("Guess: " + urllib.parse.unquote(guess))
        candidates.pop(0)
        temp_page = page + guess
        response = requests.get(temp_page)
        soup = BeautifulSoup(response.text, features='html.parser')
        guess_similarity = json.loads(soup.text)['similarity'] / 100
        if guess_similarity == 1:
            secret = urllib.parse.unquote(guess)
            break
        for c in candidates:
            if not guess_similarity - 0.2 <= model.similarity(urllib.parse.unquote(guess), c) <= guess_similarity + 0.2:
                candidates.remove(c)
        time.sleep(0.7)

    #print("The secret word is " + "\"" + secret + "\".")
    #print("I found it in " + str(guess_counter) + " guesses.")

    return(secret, str(guess_counter))

if __name__ == "__main__":
    greeting()
    guessing = True
    tic = time.perf_counter()
    t1 = threading.Thread(target = loading)
    t1.start()
    secret, guess_counter = guesser()
    toc = time.perf_counter()
    guessing = False
    t1.join()
    min, sec = divmod((toc - tic), 60)
    hrs, min = divmod(min, 60)
    print()
    print("Done!")
    print("The secret word is " + "\"" + secret + "\".")
    #print("I found it in " + guess_counter + " guesses.")
    print("I found it in " + str(round(min)) + " minutes and " + str(round(sec)) + " seconds.")